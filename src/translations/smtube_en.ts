<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>About</name>
    <message>
        <location filename="../about.ui" line="14"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.ui" line="33"/>
        <source>&amp;Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.ui" line="89"/>
        <source>&amp;Translators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.ui" line="145"/>
        <source>&amp;License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="33"/>
        <source>YouTube%1 Browser for SMPlayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="36"/>
        <source>Version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="38"/>
        <source>Portable Edition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="41"/>
        <source>Compiled with Qt %1 (using %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="43"/>
        <source>Visit our website for updates:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="86"/>
        <source>Many people contributed with translations.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="87"/>
        <source>You can also help to translate SMTube into your own language.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="88"/>
        <source>Visit %1 and join a translation team.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="90"/>
        <source>Click here to know the translators from the transifex teams</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="116"/>
        <source>&lt;b&gt;%1&lt;/b&gt; (%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BrowserWindow</name>
    <message>
        <location filename="../browserwindow.cpp" line="101"/>
        <source>Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="104"/>
        <source>Navigation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="112"/>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="113"/>
        <source>Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="117"/>
        <source>Statusbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="121"/>
        <source>&amp;Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="128"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="131"/>
        <source>&amp;Update the YouTube code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="136"/>
        <source>About this &amp;release</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="140"/>
        <source>&amp;About SMTube</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="343"/>
        <source>Connection failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="344"/>
        <source>The video you requested needs to open a HTTPS connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="345"/>
        <source>Unfortunately the OpenSSL component, required for it, is not available in your system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="350"/>
        <source>No video found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="351"/>
        <source>It wasn&apos;t possible to find the URL for this video.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="361"/>
        <location filename="../browserwindow.cpp" line="369"/>
        <source>Problems with YouTube</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="362"/>
        <location filename="../browserwindow.cpp" line="370"/>
        <source>Unfortunately due to changes in YouTube, the video &apos;%1&apos; can&apos;t be played.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="363"/>
        <source>Do you want to update the YouTube code? This may fix the problem.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="371"/>
        <source>Maybe updating this application could fix the problem.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="389"/>
        <source>About this release</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="390"/>
        <source>Due to changes in YouTube, the old SMTube doesn&apos;t work anymore.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="391"/>
        <source>This is a new version of SMTube, written from scratch.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="392"/>
        <source>Some functionality is not available yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="394"/>
        <source>Important:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="395"/>
        <source>If the VEVO videos fail to play, please use the option %1 in the Help menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="396"/>
        <source>Update the YouTube code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="211"/>
        <source>Loading... %1%</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CodeDownloader</name>
    <message>
        <location filename="../codedownloader.cpp" line="37"/>
        <source>Downloading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../codedownloader.cpp" line="58"/>
        <source>Connecting to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../codedownloader.cpp" line="126"/>
        <source>The Youtube code has been updated successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../codedownloader.cpp" line="127"/>
        <source>Installed version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../codedownloader.cpp" line="128"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../codedownloader.cpp" line="133"/>
        <location filename="../codedownloader.cpp" line="138"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../codedownloader.cpp" line="133"/>
        <source>An error happened writing %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../codedownloader.cpp" line="138"/>
        <source>An error happened while downloading the file:&lt;br&gt;%1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../configdialog.ui" line="14"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="24"/>
        <source>&amp;General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="111"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="76"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="30"/>
        <source>Playback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="36"/>
        <source>Preferred &amp;quality:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="65"/>
        <source>Players</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="122"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="146"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="87"/>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <location filename="../filechooser.cpp" line="56"/>
        <source>Click to select a file or folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyWebView</name>
    <message>
        <location filename="../mywebview.cpp" line="32"/>
        <source>Open link in a web browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mywebview.cpp" line="74"/>
        <location filename="../mywebview.cpp" line="82"/>
        <source>Open with %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mywebview.cpp" line="90"/>
        <source>Copy link to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mywebview.cpp" line="93"/>
        <source>Open link in this window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mywebview.cpp" line="96"/>
        <source>Copy text to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlayerDialog</name>
    <message>
        <location filename="../playerdialog.ui" line="14"/>
        <source>Edit Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../playerdialog.ui" line="22"/>
        <source>&amp;Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../playerdialog.ui" line="39"/>
        <source>&amp;Executable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../playerdialog.ui" line="56"/>
        <source>&amp;Parameters:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../playerdialog.ui" line="71"/>
        <source>This player supports &amp;video sites</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../players.cpp" line="101"/>
        <source>Video sites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../players.cpp" line="101"/>
        <source>Normal streams</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
