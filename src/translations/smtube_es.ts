<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>About</name>
    <message>
        <location filename="../about.ui" line="14"/>
        <source>About...</source>
        <translation>Acerca de...</translation>
    </message>
    <message>
        <location filename="../about.ui" line="33"/>
        <source>&amp;Info</source>
        <translation>&amp;Info</translation>
    </message>
    <message>
        <location filename="../about.ui" line="89"/>
        <source>&amp;Translators</source>
        <translation>&amp;Traductores</translation>
    </message>
    <message>
        <location filename="../about.ui" line="145"/>
        <source>&amp;License</source>
        <translation>&amp;Licencia</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="33"/>
        <source>YouTube%1 Browser for SMPlayer</source>
        <translation>Buscador de vídeos de YouTube%1 de SMPlayer</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="36"/>
        <source>Version: %1</source>
        <translation>Versión: %1</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="38"/>
        <source>Portable Edition</source>
        <translation>Edición Portable</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="41"/>
        <source>Compiled with Qt %1 (using %2)</source>
        <translation>Compilado con Qt %1 (usando %2)</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="43"/>
        <source>Visit our website for updates:</source>
        <translation>Visita nuestra web para conseguir actualizaciones:</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="86"/>
        <source>Many people contributed with translations.</source>
        <translation>Muchas personas han contribuido con traducciones.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="87"/>
        <source>You can also help to translate SMTube into your own language.</source>
        <translation>Tú también puedes ayudar a traducir el SMTube a tu propio idioma.</translation>
    </message>
    <message>
        <source>You can also help to translate SMPlayer into your own language.</source>
        <translation type="obsolete">Tú también puedes ayudar a traducir el SMPlayer a tu propio idioma.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="88"/>
        <source>Visit %1 and join a translation team.</source>
        <translation>Visita %1 y únete a un equipo de traducción.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="90"/>
        <source>Click here to know the translators from the transifex teams</source>
        <translation>Pulsa aquí para conocer los traductores de los equipos de transifex</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="116"/>
        <source>&lt;b&gt;%1&lt;/b&gt; (%2)</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; (%2)</translation>
    </message>
</context>
<context>
    <name>BrowserWindow</name>
    <message>
        <location filename="../browserwindow.cpp" line="101"/>
        <source>Home</source>
        <translation>Inicio</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="104"/>
        <source>Navigation</source>
        <translation>Navegación</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="112"/>
        <source>&amp;View</source>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="113"/>
        <source>Toolbar</source>
        <translation>Barra de herramientas</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="117"/>
        <source>Statusbar</source>
        <translation>Barra de estado</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="121"/>
        <source>&amp;Settings</source>
        <translation>&amp;Configuración</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="128"/>
        <source>&amp;Help</source>
        <translation>&amp;Ayuda</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="131"/>
        <source>&amp;Update the YouTube code</source>
        <translation>Actualizar el código de &amp;YouTube</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="136"/>
        <source>About this &amp;release</source>
        <translation>Acerca de esta &amp;version</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="140"/>
        <source>&amp;About SMTube</source>
        <translation>&amp;Acerca de SMTube</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="343"/>
        <source>Connection failed</source>
        <translation>Error en la conexión</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="344"/>
        <source>The video you requested needs to open a HTTPS connection.</source>
        <translation>El vídeo que has solicitado necesita abrir una conexión HTTPS.</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="345"/>
        <source>Unfortunately the OpenSSL component, required for it, is not available in your system.</source>
        <translation>Lamentablemente el componente OpenSSL, que es necesario para ello, no se encuentra en tu sistema.</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="350"/>
        <source>No video found</source>
        <translation>No se ha encontrado el vídeo</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="351"/>
        <source>It wasn&apos;t possible to find the URL for this video.</source>
        <translation>No ha sido posible encontrar la URL de este vídeo.</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="361"/>
        <location filename="../browserwindow.cpp" line="369"/>
        <source>Problems with YouTube</source>
        <translation>Problemas con YouTube</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="362"/>
        <location filename="../browserwindow.cpp" line="370"/>
        <source>Unfortunately due to changes in YouTube, the video &apos;%1&apos; can&apos;t be played.</source>
        <translation>Lamentablemente debido a cambios en YouTube, no es posible reproducir el vídeo &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="363"/>
        <source>Do you want to update the YouTube code? This may fix the problem.</source>
        <translation>¿Quieres actualizar el código de YouTube? Esto podría solucionar el problema.</translation>
    </message>
    <message>
        <source>Problems with Youtube</source>
        <translation type="obsolete">Problemas con Youtube</translation>
    </message>
    <message>
        <source>Unfortunately due to changes in Youtube, the video &apos;%1&apos; can&apos;t be played.</source>
        <translation type="obsolete">Lamentablemente debido a cambios en Youtube, no es posible reproducir el vídeo &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Do you want to update the Youtube code? This may fix the problem.</source>
        <translation type="obsolete">¿Quieres actualizar el código de Youtube? Esto podría solucionar el problema.</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="371"/>
        <source>Maybe updating this application could fix the problem.</source>
        <translation>Quizás el problema se podría solucionar actualizando esta aplicación.</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="389"/>
        <source>About this release</source>
        <translation>Acerca de esta version</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="390"/>
        <source>Due to changes in YouTube, the old SMTube doesn&apos;t work anymore.</source>
        <translation>Debido a cambios en YouTube, la versión anterior de SMTube ha dejado de funcionar.</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="391"/>
        <source>This is a new version of SMTube, written from scratch.</source>
        <translation>Esta es una versión totalmente nueva, escrita desde cero.</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="392"/>
        <source>Some functionality is not available yet.</source>
        <translation>Todavía no están disponible todas las funciones.</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="394"/>
        <source>Important:</source>
        <translation>Importante:</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="395"/>
        <source>If the VEVO videos fail to play, please use the option %1 in the Help menu.</source>
        <translation>Si los vídeos VEVO no se reproducen, por favor usa la opción %1 del menú Ayuda.</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="396"/>
        <source>Update the YouTube code</source>
        <translation>Actualizar el código de YouTube</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="obsolete">Acerca de</translation>
    </message>
    <message>
        <location filename="../browserwindow.cpp" line="211"/>
        <source>Loading... %1%</source>
        <translation>Cargando... %1%</translation>
    </message>
    <message>
        <source>About SMTube</source>
        <translation type="obsolete">Acerca de SMTube</translation>
    </message>
    <message>
        <source>This is an early version of the new SMTube.</source>
        <translation type="obsolete">Esta es una versión preliminar del nuevo SMTube.</translation>
    </message>
    <message>
        <source>Many things are still missing.</source>
        <translation type="obsolete">Todavía faltan muchas cosas.</translation>
    </message>
    <message>
        <source>License: %1</source>
        <translation type="obsolete">Licencia: %1</translation>
    </message>
    <message>
        <source>Version: %1</source>
        <translation type="obsolete">Versión: %1</translation>
    </message>
</context>
<context>
    <name>CodeDownloader</name>
    <message>
        <location filename="../codedownloader.cpp" line="37"/>
        <source>Downloading...</source>
        <translation>Descargando...</translation>
    </message>
    <message>
        <location filename="../codedownloader.cpp" line="58"/>
        <source>Connecting to %1</source>
        <translation>Conectando con %1</translation>
    </message>
    <message>
        <location filename="../codedownloader.cpp" line="126"/>
        <source>The Youtube code has been updated successfully.</source>
        <translation>El código de Youtube se ha actualizado correctamente.</translation>
    </message>
    <message>
        <location filename="../codedownloader.cpp" line="127"/>
        <source>Installed version: %1</source>
        <translation>Versión instalada: %1</translation>
    </message>
    <message>
        <location filename="../codedownloader.cpp" line="128"/>
        <source>Success</source>
        <translation>Operación realizada correctamente</translation>
    </message>
    <message>
        <location filename="../codedownloader.cpp" line="133"/>
        <location filename="../codedownloader.cpp" line="138"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../codedownloader.cpp" line="133"/>
        <source>An error happened writing %1</source>
        <translation>Ha ocurrido un error al escribir en %1</translation>
    </message>
    <message>
        <location filename="../codedownloader.cpp" line="138"/>
        <source>An error happened while downloading the file:&lt;br&gt;%1</source>
        <translation>Ha ocurrido un error descargando el fichero:&lt;br&gt;%1</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../configdialog.ui" line="14"/>
        <source>Settings</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="24"/>
        <source>&amp;General</source>
        <translation>&amp;General</translation>
    </message>
    <message>
        <source>Playback &amp;quality:</source>
        <translation type="obsolete">Calidad de re&amp;producción:</translation>
    </message>
    <message>
        <source>&amp;Players</source>
        <translation type="obsolete">&amp;Reproductores</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="111"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="76"/>
        <source>Up</source>
        <translation>Subir</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="30"/>
        <source>Playback</source>
        <translation>Reproducción</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="36"/>
        <source>Preferred &amp;quality:</source>
        <translation>Calidad &amp;preferida:</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="65"/>
        <source>Players</source>
        <translation>Reproductores</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="122"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="146"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../configdialog.ui" line="87"/>
        <source>Down</source>
        <translation>Bajar</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Nombre</translation>
    </message>
    <message>
        <source>Binary</source>
        <translation type="obsolete">Ejecutable</translation>
    </message>
    <message>
        <source>Parameters</source>
        <translation type="obsolete">Parámetros</translation>
    </message>
    <message>
        <source>Support for</source>
        <translation type="obsolete">Soporte para</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <location filename="../filechooser.cpp" line="56"/>
        <source>Click to select a file or folder</source>
        <translation>Pulsa para seleccionar un archivo o carpeta</translation>
    </message>
</context>
<context>
    <name>MyWebView</name>
    <message>
        <location filename="../mywebview.cpp" line="32"/>
        <source>Open link in a web browser</source>
        <translation>Abrir enlace en un navegador web</translation>
    </message>
    <message>
        <location filename="../mywebview.cpp" line="74"/>
        <location filename="../mywebview.cpp" line="82"/>
        <source>Open with %1</source>
        <translation>Abrir con %1</translation>
    </message>
    <message>
        <location filename="../mywebview.cpp" line="90"/>
        <source>Copy link to clipboard</source>
        <translation>Copiar enlace al portapapeles</translation>
    </message>
    <message>
        <location filename="../mywebview.cpp" line="93"/>
        <source>Open link in this window</source>
        <translation>Abrir enlace en esta ventana</translation>
    </message>
    <message>
        <location filename="../mywebview.cpp" line="96"/>
        <source>Copy text to clipboard</source>
        <translation>Copiar texto al portapapeles</translation>
    </message>
</context>
<context>
    <name>PlayerDialog</name>
    <message>
        <location filename="../playerdialog.ui" line="14"/>
        <source>Edit Player</source>
        <translation>Edtar Reproductor</translation>
    </message>
    <message>
        <location filename="../playerdialog.ui" line="22"/>
        <source>&amp;Name:</source>
        <translation>&amp;Nombre:</translation>
    </message>
    <message>
        <location filename="../playerdialog.ui" line="39"/>
        <source>&amp;Executable:</source>
        <translation>&amp;Ejecutable:</translation>
    </message>
    <message>
        <location filename="../playerdialog.ui" line="56"/>
        <source>&amp;Parameters:</source>
        <translation>&amp;Parámetros:</translation>
    </message>
    <message>
        <location filename="../playerdialog.ui" line="71"/>
        <source>This player supports &amp;video sites</source>
        <translation>Este reproductor incorpora soporte para webs de &amp;vídeos</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../players.cpp" line="101"/>
        <source>Video sites</source>
        <translation>Webs de vídeos</translation>
    </message>
    <message>
        <location filename="../players.cpp" line="101"/>
        <source>Normal streams</source>
        <translation>Datos normales</translation>
    </message>
</context>
</TS>
